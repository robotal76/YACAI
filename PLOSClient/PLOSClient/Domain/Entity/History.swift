//
//  History.swift
//  PLOSClient
//
//  Created by Vyacheslav Konopkin on 28.07.2021.
//

import Foundation

struct History: Codable, Identifiable, Equatable {
    let id: String
}
