//
//  TestError.swift
//  PLOSClientTests
//
//  Created by Vyacheslav Konopkin on 06.08.2021.
//

import Foundation

enum TestError: Error {
    case someError
}
